from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Budget, MiniBudget

class SignUpForm(UserCreationForm):
    email = forms.EmailField(max_length=254, help_text='Required. Enter a valid email address')

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')

class BudgetForm(forms.ModelForm):

    class Meta:
        model = Budget
        fields = ['name', 'amount']

class MiniBudgetForm(forms.ModelForm):

    class Meta:
        model = MiniBudget
        fields = ['name', 'amount']