from django.conf.urls import url
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^register/$', views.register, name='register'),
    url(r'^login/$', auth_views.login, {'template_name': 'login.html'}, name='login'),
    url(r'^loguot/$', auth_views.logout, {'template_name': 'login.html'}, name='logout'),
    url(r'^index', views.index, name='index'),
    url(r'^dashboard', views.dashboard, name='dashboard'),
    url(r'^create_budget', views.create_budget, name='create_budget'),
    url(r'^mini_budgets', views.mini_budgets, name='mini_budgets'),
    url(r'^detail', views.detail, name='detail')
]