from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from .forms import SignUpForm, BudgetForm, MiniBudgetForm
from .models import Budget, MiniBudget
from django.db.models import Q
from django.shortcuts import get_object_or_404

def index(request):
    return render(request, 'index.html')

def dashboard(request):
    if not request.user.is_authenticated:
        return render(request, 'login.html')
    budgets = Budget.objects.filter(user=request.user)
    # mini_budgets = MiniBudget.objects.all()
    # query = request.GET.get('q')
    # if query:
    #     budgets = budgets.filter(
    #         Q(name__icontains=query)
    #     ).distinct()
    #     mini_budget_results = mini_budgets.filter(
    #         Q(name__icontains=query)
    #     ).distinct()
    #     return render(request, 'dashboard.html', {'busgets': budgets, 'mini_budgets': mini_budget_results})
    # else:
    return render(request, 'dashboard.html', {'budgets': budgets})

def register(request):
    if not request.user.is_authenticated:
        return render(request, 'login.html')
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('dashboard')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})

def create_budget(request):
    if not request.user.is_authenticated:
        return render(request, 'login.html')
    else:
        form = BudgetForm(request.POST or None)
        if form.is_valid():
            budget = form.save(commit=False)
            budget.user_id = request.user.id
            budget.name = form.cleaned_data['name']
            budget.amount = form.cleaned_data['amount']
            budget.save()
            return render(request, 'detail.html', {'budget': budget})
    return render(request, 'create_budget.html', {'form': form})

def mini_budgets(request):
    return render(request, 'mini_budgets.html')

def detail(request):
    if not request.user.is_authenticated:
        return render(request, 'login.html')
    else:
        user =  request.user
        budget = get_object_or_404(Budget, pk=budget_id)
        return render(request, 'detail.html', {'budget': budget, 'user': user})